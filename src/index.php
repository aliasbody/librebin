<html>
    <head>
        <title>LibreBin - The Libre and Light Paste Tool</title>
        <link type="text/css" rel="stylesheet" href="css/style.css" />
        <script src="lib/js/engine.js"></script>
        <script src="lib/js/enc-base64.js"></script>
        <script src="lib/js/aes.js"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    </head>

    <body>

    <div id="Header">
        <a href="https://www.libretrend.com/">
            <img src="../store/image/data/Logo/LTD_Logo.png" alt="Logo Image"/>
        </a>    
    </div>
    
    <div id="MainText">
        <div id="Menu">
            <hr/>
            <button id="btnEncrypt" OnClick="SendMessage(textField, FinalURL);" class="visible">Encrypt</button>
            <button id="btnNew" OnClick="location.href='index.php'" class="hidden">New</button>
            <hr/>
        </div>
        
        <div id="MainURL">
            <div id="FinalURL" class="finalURL"></div>
        </div>
        
        <div id="Content">
            <textarea id="textField" name="textField" required="true"></textarea>
        </div>
        
        <hr />
    </div>

    <div id="Footer">
        <a href="https://www.libretrend.com">LibreTrend</a>
        |
        <a href="https://gitorious.org/libretrend/librebin/">Source Code</a>
        |
        <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=LMRULGA99PKN6">Donate</a>        
    </div>

    <?php
        function createFile($filePath) {
            $fileHandle = fopen($filePath, 'w') or die ("Can't open file");  
            fwrite($fileHandle, nl2br(htmlspecialchars($_POST['encryptedMsg'])));
            fclose($fileHandle);
        }
        
        if (isset($_POST['fileName']) && isset($_POST['encryptedMsg'])) {
            createFile("storage/data/" . $_POST['fileName'] . ".txt");
        } elseif (isset($_GET['MessageID']) && isset($_GET['Key'])) {            
            $fileContent = file_get_contents("storage/data/" . $_GET['MessageID'] . ".txt");
            echo "<script>btnEncrypt.className = 'hidden';</script>";
            echo "<script>btnNew.className    = 'visible';</script>";
            
            echo "<script>textField.value = decrypt(decodeBase64('" . $fileContent . "'), '" . $_GET['Key'] . "');</script></br>";
        }
    ?>

    </body>
</html>
