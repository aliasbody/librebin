/*
    Copyright (c) 2014 by LibreTrend <admin@libretrend.com>


    This file is part of LibreBin.

    LibreBin is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    LibreBin is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with LibreBin.  
    
    If not, see <http://www.gnu.org/licenses/>.
*/

// Encrypt the message using a generated key
function encrypt(message, key) {
    return CryptoJS.AES.encrypt(message, key);
}

// Encode String to Base64
function encodeBase64(value) {
    //return CryptoJS.enc.Base64.parse(value.toString());
    return btoa(value);
}

// Decode String from Base64 Enconding
function decodeBase64(value) {
    //return CryptoJS.enc.Base64.stringify(encodedValue);
    return atob(value);
}

// Decrypt the message using the generated key
function decrypt(encrypted, key) {
    return CryptoJS.AES.decrypt(encrypted, key).toString(CryptoJS.enc.Utf8);
}

// Generate the random key
function generateKey() {
    return CryptoJS.lib.WordArray.random(16).toString();
}

// Generate the random fileName
function generateFileName() {
    return CryptoJS.lib.WordArray.random(16).toString();
}

// Convert the text on the form with the encrypted version to be sent into the server
function SendMessage(message, FinalURL) {
    if ((message.value).trim()) {
        var xmlhttp = new XMLHttpRequest;
        xmlhttp.open("POST", "index.php", true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        // Generate the Key and Encrypt the Message
        var key             = generateKey();
        var encryptedData   = encrypt(message.value, key);
        var fileName        = generateFileName();      

        xmlhttp.send("fileName=" + fileName + "&encryptedMsg=" + encodeBase64(encryptedData));
        
        var finalURL = document.URL + "?MessageID=" + fileName + "&Key=" + key;

        FinalURL.innerHTML = "<p>Final URL: <a href=" + finalURL + ">" + finalURL + "</a></p>";
    } else {
        alert("There is no text to be encrypted !");
    }
}
